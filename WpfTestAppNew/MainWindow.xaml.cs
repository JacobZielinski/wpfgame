﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfTestAppNew
{
    public partial class MainWindow : Window
    {
       
        public MainWindow()
        {
            InitializeComponent();
            
            

        }

        class MainHero
        {
            public static string name = "";
            public static string warrior = "Warrior";
            public static string tank = "Tank";
            public static string mage = "Mage";
            public static int lvl = 1;
            public static int gold = 0;
            public static int food = 6;
            public static int sleep = 6;
            public static int turns = 1;
            public static int exp = 0;
            public static int turnsForInfo = 1;

            public static int str = 0;
            public static int def = 0;
            public static int intel = 0;

        }

        class Enemy
        {
            public static string name = "";
            public static int str = 0;
            public static int def = 0;
            public static int intel = 0;
            public static int xp = 5;
            public static int gold = 0;
           

        }
        public void GameLogic()
        {
           if ( MainHero.sleep < 1  || MainHero.food < 1)
            {
                info.Text = $"{MainHero.name} lived for {MainHero.turns} days and died due horrific case of negligence";
                GameOver();

            }
            
            
        }

        public void GameOver()
        {
            button1.IsEnabled = false;
            button2.IsEnabled = false;
            button4.IsEnabled = false;
            button.IsEnabled = true;
            info.Text = "...";
            Info2.Text = "...";
            Info3.Text = "...";
            Info4.Text = "..."; 
            MainHero.food = 6;
            MainHero.sleep = 6;
            MainHero.turns = 0;
            MainHero.str = 0;
            MainHero.def = 0;
            MainHero.intel = 0;
            Enemy.str = 0;
            Enemy.def = 0;
            Enemy.intel = 0;
            enemyStr.Text = $"Strength: ";
            enemyDef.Text = $"Defence: ";
            enemyIntel.Text = $"Intelligence: ";
            sleep.Text = "Tiredness: " + MainHero.sleep.ToString();
            food.Text = "Hunger: " + MainHero.sleep.ToString();
            turn.Text = "Turns: " + MainHero.turns.ToString();

        }


        private void Start(object sender, RoutedEventArgs e)
        {

            MainHero.name = textBox.Text;
            textBlock.Text = $"Name: {MainHero.name}";
            if (MainHero.name == "")
            {
                info.Text = "Please Enter Name";
            }
            else
            {
                RandomStats();
                level.Text = $"Level: {MainHero.lvl}";
                gold.Text = $"Gold: {MainHero.gold}";
                button1.IsEnabled = true;
                button2.IsEnabled = true;
                info.Text = $"Day 0 Hero {MainHero.name} was born.";
                button.IsEnabled = false;
                str.Text = $"Strength: {MainHero.str}";
                def.Text = $"Defence: {MainHero.def}";
                intel.Text = $"Intelligence: {MainHero.intel}";
                textBox.IsEnabled = false;
                if(MainHero.str > 5)
                {
                    classType.Text = $"Class: {MainHero.warrior}";
                    character.Source = new BitmapImage(new Uri(@"C:\Users\Kuba\Desktop\appr\projects\Img\warrior.jpeg"));
                }
                else if(MainHero.def > 5)
                {
                    classType.Text = $"Class: {MainHero.tank}";
                    character.Source = new BitmapImage(new Uri(@"C:\Users\Kuba\Desktop\appr\projects\Img\tank.jpg"));
                }
                else
                {
                    classType.Text = $"Class: {MainHero.mage}";
                    character.Source = new BitmapImage(new Uri(@"C:\Users\Kuba\Desktop\appr\projects\Img\mage.gif"));
                }


            }

        }

        public void Feed(object sender, RoutedEventArgs e)
        {           
            GameLogic();
            Feeding();
        }

        private void Sleep(object sender, RoutedEventArgs e)
        {      
            GameLogic();
            Sleeping();
        }

      

        public void Feeding()
        {
            MainHero.turns++;
            MainHero.sleep--;
            sleep.Text = "Tiredness: " + MainHero.sleep.ToString();
            turn.Text = "Turns: " + MainHero.turns.ToString();
            int feedingButton = MainHero.food++;
            food.Text = "Hunger: " + feedingButton.ToString();
        }

        public void Sleeping()
        {
            MainHero.turns++;
            MainHero.food--;
            food.Text = "Hunger: " + MainHero.food.ToString();
            turn.Text = "Turns: " + MainHero.turns.ToString();
            int sleepingButton = MainHero.sleep++;
            sleep.Text = "Tiredness: " + sleepingButton.ToString();
            imageMonster.Source = new BitmapImage(new Uri(@"C:\Users\Kuba\Desktop\appr\projects\Img\grass2.png"));
        }

        public void IntelligenceLogic()
        {
            Random luck1 = new Random();
            int luck = luck1.Next(1, 3);

            if (MainHero.intel / luck > 1)
            {
                str.Text = $"Strength: {MainHero.str} + {luck}";
                def.Text = $"Defence: {MainHero.def} + {luck}";
            }
        }

        public void NextTurn()
        {
            Levelup();
            if (MainHero.turnsForInfo == 1)
            {
                info.Text = $"Day {MainHero.turns}, {MainHero.name} was walking through the Swamps.";
            }
            else if (MainHero.turnsForInfo == 2)
            {
                Info2.Text = $"Day {MainHero.turns}, {MainHero.name} was walking through the forest.";
            }else if (MainHero.turnsForInfo == 3)
            {
                Info3.Text = $"Day {MainHero.turns}, {MainHero.name} was walking through the Mountains.";
            } else if (MainHero.turnsForInfo == 4)
            {
                Info4.Text = $"Day {MainHero.turns}, {MainHero.name} was walking through the Desert.";
                MainHero.turnsForInfo = 0;
            }
            MainHero.turnsForInfo++;
            Random counter = new Random();
            int encounter = counter.Next(1, 5);
            switch (encounter)
            {
                case 1:
                    if (MainHero.lvl < 4)
                    {
                        
                        EasyEnemy();
                        button4.IsEnabled = false;
                        button5.IsEnabled = true;

                        attackButton.IsEnabled = true;

                        //Attack();
                        //imageMonster.Source = "";
                        //imageMonster.SetValue(IsVisibleProperty, true);
                        imageMonster.Source = new BitmapImage(new Uri(@"C:\Users\Kuba\Desktop\appr\projects\Img\monster.jpg"));

                    }
                    else if(MainHero.lvl > 4 && MainHero.lvl < 11)
                    {
                        MediumEnemy();
                    }
                    break;
                case 2:

                    break;
                default:

                    break;
            }
           

        }

        public void Levelup()
        {
            if(MainHero.exp == 15)
            {
                MainHero.lvl = 2;
                MainHero.str = MainHero.str + 2;
                MainHero.def = MainHero.def + 2;
                MainHero.intel = MainHero.intel + 2;
                str.Text = $"Strength: {MainHero.str}";
                def.Text = $"Defence: {MainHero.def}";
                intel.Text = $"Intelligence: {MainHero.intel}";
                level.Text = $"Level: {MainHero.lvl}";
                info.Text = $"You have reached level {MainHero.lvl}";
                experience.Text = $"Experince: {MainHero.exp}";

            }
        }
        public void Attack()
        {
            Random luck2 = new Random();
            int luck3 = luck2.Next(0, 2);
            Levelup();

            if (MainHero.intel < 5)
            {
                luck3++;
                luck3++;

            }
            else if (MainHero.intel > 3)
            {
                luck3 = 0;
            }
            int strVsDefMyTurn = Enemy.def - MainHero.str + luck3;
            int strVsDefEnemyTurn = MainHero.def + luck3 - Enemy.str;
            myTurn.Text = $"Enemy HP {strVsDefMyTurn}";
            enemyTurn.Text = $"My HP {strVsDefEnemyTurn}";

            if (strVsDefMyTurn < strVsDefEnemyTurn)
            {


                Enemy.str = 0;
                Enemy.def = 0;
                Enemy.intel = 0;
                MainHero.exp = MainHero.exp + Enemy.xp;


                //NextTurn();
                attackButton.IsEnabled = false;
                button4.IsEnabled = true;
                imageMonster.Source = new BitmapImage(new Uri(@"C:\Users\Kuba\Desktop\appr\projects\Img\grass2.png"));

            }
            else
            {
                attackButton.IsEnabled = false;
                imageMonster.Source = new BitmapImage(new Uri(@"C:\Users\Kuba\Desktop\appr\projects\Img\grass2.png"));
                GameOver();
                info.Text = "You Died";
            }

        }
        public void EasyEnemy()
        {
            Random easyEnemy = new Random();
            while (Enemy.str + Enemy.def + Enemy.intel < 12)
            {

                int easy = easyEnemy.Next(1, 12);
                if (easy < 3)
                {
                    Enemy.str++;
                }
                else if (easy > 4 && easy < 9)
                {
                    Enemy.def++;
                }
                else
                {
                    Enemy.intel++;
                }
            }
            enemyStr.Text = $"Strength: {Enemy.str}";
            enemyDef.Text = $"Defence: {Enemy.def}";
            enemyIntel.Text = $"Intelligence: {Enemy.intel}";
        }

        public void MediumEnemy()
        {
            Random mediumEnemy = new Random();
            while (Enemy.str + Enemy.def + Enemy.intel < 18)
            {

                int medium = mediumEnemy.Next(1, 12);
                if (medium < 3)
                {
                    Enemy.str++;
                }
                else if (medium > 4 && medium < 9)
                {
                    Enemy.def++;
                }
                else
                {
                    Enemy.intel++;
                }
            }
            enemyStr.Text = $"Strength: {Enemy.str}";
            enemyDef.Text = $"Defence: {Enemy.def}";
            enemyIntel.Text = $"Intelligence: {Enemy.intel}";
        }
        public void RandomStats()
        {
            button4.IsEnabled = true;
            Random r = new Random();
            while ( MainHero.str + MainHero.def + MainHero.intel < 15)
            {

                int n = r.Next(1, 15);
                if (n < 6)
                {
                    MainHero.str ++;
                }else if(n > 5 && n < 11)
                {
                    MainHero.def++;
                }
                else
                {
                    MainHero.intel++;
                }
            }
        }

        private void Button4_Click(object sender, RoutedEventArgs e)
        {
            NextTurn();
          
            turn.Text = $"Turns: {MainHero.turns++}";
        }

        private void Button3_Click(object sender, RoutedEventArgs e)
        {
            Attack();
            button5.IsEnabled = false;
            enemyStr.Text = $"Strength: ";
            enemyDef.Text = $"Defence: ";
            enemyIntel.Text = $"Intelligence: ";

        }

        private void Button5_Click(object sender, RoutedEventArgs e)
        {
            Random escape = new Random();
            int escapeout = escape.Next(0, 5);
            attackButton.IsEnabled = false;
            if(escapeout < 1)
            {
                Attack();
                enemyStr.Text = $"Strength: ";
                enemyDef.Text = $"Defence: ";
                enemyIntel.Text = $"Intelligence: ";
            }
            else
            {
                info.Text = "You managed to escape!";
                enemyStr.Text = $"Strength: ";
                enemyDef.Text = $"Defence: ";
                enemyIntel.Text = $"Intelligence: ";
            }
            button5.IsEnabled = false;
            button4.IsEnabled = true;



        }
    }
}
;